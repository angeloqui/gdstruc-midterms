#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>
#include <vector>

using namespace std;

void displayArray(UnorderedArray<int> unordered, OrderedArray<int> ordered) {
	cout << "Unordered contents: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << " ";
	cout << "\nOrdered contents: " << string(2, ' ');
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << " ";
	cout << endl;
}

void searchArray(int results[2], int input, int arrayType, UnorderedArray<int> unordered, OrderedArray<int> ordered) {
	
	if (results[arrayType] >= 0) {
		if (arrayType == 0) 
			cout << "\nLinear Search took " << unordered.getCounter();
		else 
			cout << "\nBinary Search took " << ordered.getCounter();
		cout << " comparisons.";
	}
	
	//Display only on the second loop
	if (arrayType == 1) {
		cout << "\nElement " << input << " found. ";
		
		for (int i = 0; i < 2; i++) {
			if (results[i] >= 0) {
				cout << "Index " << results[i];
				if (i == 0) 
					cout << " for UnorderedArray";
				else 
					cout << " for OrderedArray.";
				
				//check if comma is needed, refer to condition on line 102
				//crude way of doing things, but it works
				if (results[2] == 1) {
					cout << ", ";
					results[2] = 0;	//display only once
				}
			}
		}
	}
}

int main() {
	
  	srand (time(NULL));
  
	cout << "Enter size for dynamic arrays: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++) {
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	
	system("cls");
	
	while (true) {

		displayArray(unordered, ordered);
		
		int option;
		cout << "\nWhat do you want to do?"
		     << "\n1 - Remove element at index"
		     << "\n2 - Search for element"
		     << "\n3 - Expand and generate random values" << endl;
		cin >> option;
		
		int input;
		if (option == 1) {
			cout << "\n\nEnter index to remove: ";
			cin >> input;
			
			unordered.remove(input);
			ordered.remove(input);
			
			displayArray(unordered, ordered);
		} else if (option == 2) {
			cout << "\n\nInput element to search: ";
			cin >> input;
			
			int result = unordered.linearSearch(input);
			int	result2 = ordered.binarySearch(input);
			
			//few fixes, i modified the version where
			//if one result has an element equal to input
			//it will display only one array and not "not found"
			int results[3] = {result, result2, 0};
			if (results[0] >= 0 && results[1] >= 0) results[2] = 1;
			
			if (result >= 0 || result2 >= 0)
				for (int i = 0; i < 2; i++)
					searchArray(results, input, i, unordered, ordered);			
			else
				cout << "\nElement " << input << " not found." << endl;
			cout << endl;
		} else if (option == 3) {
			int n;
			
			cout << "\n\nInput size of expansion: ";
			cin >> n;
			
			for (int i = 0; i <= n; i++) {
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}
			
			displayArray(unordered, ordered);
		}
		
		system("pause");
		system("cls");
	}
}
