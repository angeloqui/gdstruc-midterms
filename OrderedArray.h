#pragma once
#include <assert.h>
#include <iostream>

using namespace std;

template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0), counter(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();
		
		mArray[mNumElements] = value;
		mNumElements++;
		
		T temp;
		for (int i = mNumElements - 1; i > 0; i--) {
			if (mArray[i] < mArray[i - 1]) {
				temp = mArray[i];
				mArray[i] = mArray[i - 1];
				mArray[i - 1] = temp;
			}
		}
	}

	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T & operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}


	virtual int binarySearch(T val)
	{
		counter = 0;
		int size = mNumElements - 1;
		int index = 0;
		
		while (index <= size) {
			counter++;	
			int mid = (index + size) / 2;
			/* IF TESTING, math is hard
			cout << "index: " << mArray[index] << " mid: " << mArray[mid] << endl;
			system("pause");
			*/
			if (val == mArray[mid]) return mid;
			if (mArray[mid] < val) index = mid + 1;
			else size = mid - 1;
		}
		
		return -1;
	}
	
	int getCounter () {
		return counter;
	}

private:
	T * mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;
	int counter;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T * temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}
};
